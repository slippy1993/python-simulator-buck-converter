import numpy as np
import matplotlib.pyplot as plt
import random
import math
import scipy
from scipy.integrate import odeint
from matplotlib import interactive



class Solver2():
    def __init__(self,duty_cycle,initial_voltage,initial_current,U_in,R,L,C,switching_frequency):
        self.duty_cycle = duty_cycle
        self.initial_voltage = initial_voltage
        self.initial_current = initial_current
        self.U_in = U_in
        self.R = R
        self.L = L
        self.C = C
        self.switching_frequency = switching_frequency
        self.period_time = 1 / self.switching_frequency

        self.state_observation = np.zeros((2,))
        self.ON_TIME = self.period_time * self.duty_cycle
        self.OFF_TIME = (1 - self.duty_cycle) * self.period_time

        self.I_L = 0
        self.U_R = 0
        self.integral_I = 0
        self.integral_U = 0
        self.times = [0]
        self.totalV = 1


    def change_values_of_simulation(self,U_in,R,L,C,switching_frequency):
        self.U_in = U_in
        self.R = R
        self.L = L
        self.C = C
        self.switching_frequency = switching_frequency
        self.period_time = 1/ self.switching_frequency


    def reset(self):
        #self.times = [0]
        self.U_R = 0
        self.I_L = 0
        self.integral_U = 0
        self.integral_I = 0
        self.ON_TIME = 0
        self.OFF_TIME = 0

    def ON_state(self,Z,t):
        I = Z[0]
        U = Z[1]
        dIdt = (-1 / self.L) * U + (1 / self.L) * self.U_in
        dUdt = (1 / self.C) * I - (1 / (self.R * self.C)) * U  # self.U_R#U_R#self.U_R
        dzdt = [dIdt, dUdt]
        return dzdt


    def OFF_state(self,Z,t):
        I = Z[0]
        U = Z[1]
        dIdt = 0 - (U / self.L)
        dUdt = (1 / self.C) * I - (1 / (self.R * self.C)) * U #U_R
        dzdt = [dIdt, dUdt]
        return dzdt

    def DCM_state(self,Z,t):
        I = Z[0]
        U = Z[1]
        dIdt = 0*I +0*U + 0* self.U_in
        dUdt = 0*I - U/(self.C*self.R) + 0*self.U_in
        dzdt = [dIdt, dUdt]
        return dzdt









    def simulation_handler(self,action):
        self.duty_cycle = round(float(action),4)
        self.ON_TIME = self.period_time * self.duty_cycle
        self.OFF_TIME = (1 - self.duty_cycle) * self.period_time
        self.MODE = "CCM"

        voltagelist = []
        currentlist = []


        if self.MODE == "CCM":
            t = np.linspace(0, self.ON_TIME)
            to_append = self.times[-1] + t[-1]
            self.times.append(to_append)

            #On TIME
            self.z0 = [self.I_L, self.U_R]
            z = odeint(self.ON_state,self.z0,t)
            self.I_L = z[-1][0]
            self.U_R = z[-1][1]
            currentlist.append(self.I_L)
            voltagelist.append(self.U_R)


            #OFF TIME to calculate intersection
            self.z0 = [self.I_L, self.U_R]
            t = np.linspace(0, self.OFF_TIME)
            z = odeint(self.OFF_state, self.z0, t)
            intersecttime = 0
            intersect_index = 0
            DCM = 0
            for i in range(0,len(z-1)):
                if(z[i][0]<0 and z[i-1][0]>=0):
                    intersecttime = (-z[i][0]+((z[i][0]-z[i-1][0])/(t[i]-t[i-1]))*t[i])/((z[i][0]-z[i-1][0])/(t[i]-t[i-1]))
                    intersect_index = i
                    DCM = 1
                    break

                else:
                    DCM = 0 #no dcm detectd
                    pass




            #OFF TIME inclusive possible DCM

            if(DCM == 0): #no DCM
                self.I_L = z[-1][0]
                self.U_R = z[-1][1]
                currentlist.append((self.I_L))
                voltagelist.append(self.U_R)
                to_append = self.times[-1] + t[-1]
                self.times.append(to_append)

            elif(DCM == 1): #DCM detetcted                          #CURRENT GETS REALLY HIGH, WHY???
                DCM = 0
                self.z0 = [self.I_L, self.U_R]
                t = np.linspace(0, intersecttime,num=intersect_index)
                to_append = self.times[-1] + t[-1]
                self.times.append(to_append)
                z = odeint(self.OFF_state, self.z0, t)
                self.I_L = 0.0#z[-1][0] #0
                self.U_R = z[-1][1]
                currentlist.append(self.I_L)
                voltagelist.append(self.U_R)

                self.z0 = [self.I_L, self.U_R]
                t = np.linspace(0, self.OFF_TIME-intersecttime, num=50-intersect_index)
                to_append = self.times[-1] + t[-1]
                self.times.append(to_append)
                z = odeint(self.DCM_state, self.z0, t)
                self.I_L = z[-1][0]
                self.U_R = z[-1][1]
                currentlist.append(self.I_L)
                voltagelist.append(self.U_R)
            self.totalV += len(voltagelist)

        return voltagelist,currentlist,self.times#self.U_R, self.I_L














 sol2 = Solver2(0.5,12,2.4,24,40,0.00005,0.00001,100000)
 vl2,il2,t2 = [], [], []
 vl2.append(0)
 il2.append(0)
 times = np.linspace(0,1, num=10000)
 for i in range(0,100):
     if(i <=49):
         #sol1.change_values_of_simulation(24,20,0.00005,0.00001,100000)
         sol2.change_values_of_simulation(24, 20, 0.00005, 0.00001, 100000)
     elif(i==50):
         #sol1.change_values_of_simulation(24,5,0.00005,0.00001,100000)
         sol2.change_values_of_simulation(24, 5, 0.00005, 0.00001, 100000)
     elif(i>=70):
         #sol1.change_values_of_simulation(24, 10, 0.00005, 0.00001, 100000)
         sol2.change_values_of_simulation(24, 10, 0.00005, 0.00001, 100000)

    
     v2, i2, t2 = sol2.simulation_handler(0.3)

     
     vl2.append(v2[0])
     vl2.append(v2[1])

     try:
        # vl.append(v[2])
         vl2.append(v2[2])
     except:
         pass
     #il.append(i[0])
     #il.append(i[1])
     il2.append(i2[0])
     il2.append(i2[1])
     try:
         #il.append(i[2])
         il2.append(i2[2])
     except:
         pass

 #plt.plot(t,vl)
 plt.plot(t2,vl2)
 plt.show()

